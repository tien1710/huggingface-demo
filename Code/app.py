from flask import Flask
from flask import request
from transformers import pipeline
classifier = pipeline('sentiment-analysis')
app = Flask(__name__)

@app.route('/')
def hello_world():
    args = request.args
    results = classifier(args['text'])
    return f"label: {results[0]['label']}, with score: {round(results[0]['score'], 4)}"