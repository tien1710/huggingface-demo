# huggingface-demo
Build API to run model from huggingface

# Setup Environment
- Refer Docs/Guide.txt

# Dev with Developent Container
- Clone source code
- Run Docker Desktop
- Run VS Code
- From VS Code: New Window -> Open Remote Window -> Remote Containers: Open Folder in Container... -> Folder Code
- Waiting build image...
- Run app: python3 -m flask run
- Test: http://localhost:5000/?text=Happy new year

# Run with Docker
- Clone source code
- Run Docker Desktop
- Run VS Code
- From VS Code: Open Terminal
- Build image: docker build --tag huggingface
- Run container: docker run -d -p 5000:5000 huggingface
- Test: http://localhost:5000/?text=Happy new year
